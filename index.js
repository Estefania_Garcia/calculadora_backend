//Importar la clase express

import express from 'express';


//Crear un objeto express
const app=express();
const puerto=3001;

// Crear una ruta

app.get("/bienvenida", (req, res)=>{

    res.send("Bienvenido al mundo de backend Node.");
});

app.get("/sumar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)+Number(peticion.query.b);

    respuesta.send(resultado.toString());
});
app.get("/restar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)-Number(peticion.query.b);

    respuesta.send(resultado.toString());
});

app.get("/multiplicar", (peticion, respuesta)=>{

    let resultado=(Number(peticion.query.a))*(Number(peticion.query.b));

    respuesta.send(resultado.toString());
});
app.get("/dividir", (peticion, respuesta)=>{

    if(Number(peticion.query.b)!=0){
    let resultado=Number(peticion.query.a)/Number(peticion.query.b);
    respuesta.send("El resultado de la multiplicacion de los numero indicados es: "+resultado.toString());
}
if(Number(peticion.query.b)==0){
    respuesta.send("Math ERROR.....No es posible dividir entre cero");
}
});

app.get("/modular", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)%Number(peticion.query.b);

    respuesta.send("El residuo de la divicion de los numeros es: "+resultado.toString());
});

//Inicializar el servidor node

app.listen(puerto, ()=>{console.log("Esta funcionando el servidor.")});




